// import {EventEmitter} from 'events';
import EventEmitter from './EventEmitter';
export const TICK: string = "tick";
export const START: string = "start";
export const STOP: string = "stop";

export default class Clock extends EventEmitter {

    private interval: NodeJS.Timeout = setInterval(() => { }, 0);
    private counter: number;
    private clockTime: number;
    
    constructor(ms: number) {
        super();
        this.clockTime = ms;
        this.counter = 0;
    }

    tick(): void {
        this.counter++;
        super.emit(TICK, this.counter);
    }
    start(): void {
        const bindedTick = this.tick.bind(this);

        this.interval = setInterval(bindedTick, this.clockTime);
        super.emit(START);

    }
    stop(): void {
        clearInterval(this.interval);
        super.emit(STOP);

    }

}
