import Clock, {TICK,START,STOP} from "./Clock";
import marker from '@ajar/marker';
type lisetnerFn = (...args: any) => void;

// initialize clock and assign number of ms between ticks
const casio = new Clock (1000);
type listenerCb = (...args:any) => void;

// My "on" implementation, named "attach"
casio.on(TICK,handleTick)
casio.once(START,handleStart)
casio.once(STOP,handleStop)


function handleStart() : void{
    marker.blue("Start");
}

function handleStop():void{
    marker.red("End");
}
function handleTick(counter:number):void {
    console.log(`The clock ticking ${counter} times`)    

    if(counter > 10) {
        casio.stop();
    }
}

casio.start();