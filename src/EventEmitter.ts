import log from "@ajar/marker";


type listenerCb = (...args:any) => void;

interface Events {
    [key: string]: listenerCb[];
  }

export default class EventEmitter {

  private listeneObj: Events;

  constructor(){
    this.listeneObj = {};
  }
    /**
     * Essential tasks
     **/
    on(eventName :string,callback:listenerCb):EventEmitter {

      this.listeneObj[eventName] = this.listeneObj[eventName] || [];
      this.listeneObj[eventName].push(callback);
            
      //adds a callback to be called when an event occures
      return this;
    }
    off(eventName:string, callback:listenerCb) : EventEmitter {
      //removes a callback from a given event name
      this.listeneObj[eventName] = this.listeneObj[eventName].filter((cb)=>{
        return cb !== callback;
      });

      return this;
    }
    emit(eventName:string , ...payload: any): boolean {
      //calls all callbacks registered to a given event
      //takes an event name, and any payload/s to pass to its registered callbacks.

        this.listeneObj[eventName].forEach((cb)=>{
          cb(...payload);
        });

      return true;
    }
  
    /**
     * Bonus tasks
     **/
    once(eventName:string,callback:listenerCb):EventEmitter {
      // adds a callback to be called when an event occures
      // same as `on` but happens only once!

      this.listeneObj[eventName] = this.listeneObj[eventName] || [];

      const onceScript = (...payload: any) => {
        callback(...payload);
        this.off(eventName, onceScript);
      }
      
      this.listeneObj[eventName].push(onceScript);
    

      return this;
    }
    listeners(eventName : string):listenerCb[] {
      // returns ALL callbacks of a given event name

      return this.listeneObj[eventName];
    }
    eventNames(): string[] {
      // returns ALL event names
      return Object.keys(this.listeneObj);
    }
    removeAllListeners(eventName:string) : EventEmitter {
      // removes all listeners of a given event name
      this.listeneObj[eventName] =[];
      return this;
    }
  }
  